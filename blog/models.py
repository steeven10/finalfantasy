from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class User(AbstractUser):
    fullname = models.CharField(max_length=50)
    email = models.EmailField()

class Game(models.Model):
    name= models.CharField(max_length=50)
    description = models.TextField()
    date= models.DateField()

    def __str__(self):
        return self.name

class Movie(models.Model):
    name= models.CharField(max_length=50)
    synopsis = models.TextField()
    date = models.DateField()
    name_game = models.ForeignKey('Game', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
class Job(models.Model):
    sex = (
        ('Female','Female'),
        ('Male','Male'),
        ('Unisex','Unisex')
    )
    name = models.CharField(max_length=50)
    description = models.TextField()
    sexo = models.CharField(max_length=20,choices=sex)
    def __str__(self):
        return self.name

class Specie(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    def __str__(self):
        return self.name

class Character(models.Model):
    sex = (
        ('Female','Female'),
        ('Male','Male'),
        ('Unisex','Unisex')
    )
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    sexo = models.CharField(max_length=20, choices=sex)
    history = models.TextField()
    name_game = models.ForeignKey('Game', on_delete=models.CASCADE)
    name_movie = models.ForeignKey('Movie', on_delete=models.CASCADE)
    name_job = models.ForeignKey('Job', on_delete=models.CASCADE)
    name_specie = models.ForeignKey('Specie',on_delete=models.CASCADE)

    def __str__(self):
        return self.name

