from django.contrib import admin
from blog.models import User, Movie, Character, Specie, Game, Job


# Register your models here.
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    """ Class to control User admin interface """


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    """ Class to control Movie admin interface """
    list_display = ('name', 'synopsis', 'date', 'name_game')


@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    """ Class to control Character admin interface """
    list_display = ('name', 'age', 'sexo', 'history', 'name_game', 'name_movie', 'name_job', 'name_specie')

@admin.register(Specie)
class SpecieAdmin(admin.ModelAdmin):
    """ Class to control Spicie admin interface """
    list_display = ('name', 'description')

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    """ Class to control Game admin interface """
    list_display = ('name', 'description', 'date')


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    """ Class to control Job admin interface """
    list_display = ('name', 'description', 'sexo')
